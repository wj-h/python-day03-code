import pymysql

db = pymysql.connect(
    host="localhost",
    port=3306,
    user="root",
    passwd="123456",
    autocommit=True,
    database="testdb"
)

cursor = db.cursor()
# 预处理
# cursor.execute("")

sql = """   
      
      """
try:
    # 执行sql
    cursor.execute(sql)
except:
    # 发生错误后回滚
    db.rollback()

db.close()
