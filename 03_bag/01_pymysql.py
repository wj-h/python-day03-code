from pymysql import Connection
# import pymysql
# 创建数据库连接
coon = Connection(
    host="localhost",
    port=3306,
    user="root",
    passwd="123456",
    autocommit=True,
    database="testdb",
)
# 尝试打印连接的数据库版本
# print(coon.get_server_info())
cursor = coon.cursor() #获取游标对象
# 选择数据库
# coon.select_db("testdb")
# 执行sql 查寻全部数据
# cursor.execute("CREATE TABLE test_pymysql(id INT)")

# 查询sql语句
cursor.execute("select * from user")
results = cursor.fetchall()
for r in results:
    print(r)

# cursor.execute("insert into user values(3, '王五','455','5435') ")
# 关闭数据库连接
coon.close()
