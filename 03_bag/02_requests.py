import requests
# 伪装成用户
headers = {
    "User-Agent" : "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36 Edg/122.0.0.0"
}
response = requests.get("https://movie.douban.com/top250", headers = headers)
# 测试连接
# print(response)
# print(response.status_code)
if response.ok:
    print(response.text)
else:
    print("请求失败")