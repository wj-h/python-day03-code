print("Hello World")
A = 45;
B = 12;
C = "jkfsdj"
print(A)
print(C)
SC = [1, 2, 3, 4, 5, 6, 7, 8, 9]  # 列表 可修改元素值
SC02 = (1, 2, 3, 4, 5, 6, 7, 8, 9)  # 元组 不可修改元素值
print(SC02)
print(SC02[0])
print(SC02[-1])
SC[0] = 7
print(SC[0])

dick = {"name": "John", "age": 18}
print(dick["name"])
# bin(20)
print(bin(20))  # 2进制
print(oct(20))  # 8进制
print(hex(20))  # 16进制
print(type(SC[1]))  # 打印出数据类型
print(1.2e5)  # 浮点数
print()
# in
# not in
a = 1;
b = 2;
list1 = [1, 2, 3];
list2 = [4, 5]
if a in list1:
    print("在")
else:
    print("不在")
    print()
if a not in list1:
    print("在")
else:
    print("不在")
print(6 & 11)
print(bin(6))
print(bin(11));
print(bin(2))
print()
print(bin(2 << 2))
print(2 << 2)  # 2*2的n次方
print(2 >> 2)  # 2*2的-n次方
print()
print(bin(8 & 3))  # 按位与 11取1
print(bin(8 | 3))  # 按位或 01或11取1
print()
print(bin(8 ^ 3))  # 按位异或 01取1
print(bin((~9)))  # 按位取反 0取1;1取0
print(bin((~8)))
print(bin((~7)))
print()
print(bin(0b00001000 >> 2))
print()