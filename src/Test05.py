# # 循环遍历
# names_list = ['xaioming1', 'xaioming2', 'xiaoming3', 'xaioming4', ]
# # for
# for name in names_list:
#     print(name)
# print()
#
# # while
# length = len(names_list)
# i = 0
# while i < length:
#     print(names_list[i])
#     i += 1
#
# print('------------------添加之前的数据-------------------')
# for temp in names_list:
#     print(temp)
#
# temp_name = input('请输入要添加的内容')
# names_list.append(temp_name)
# # append往列表里添加一个元素
# # extend将一个列表里的元素逐一添加到另外一个列表里
# # insert(0,object)往下标为0的地方插入object
# print('------------------添加之后的数据--------------------')
# for temp in names_list:
#     print(temp)
#
# list_dome = ['xaioming1', 'xiaohong']
# print('-----------------修改之前的数据---------------------')
# for temp in list_dome:
#     print(temp)
#  修改 list_dome 中的数据
# list_dome[1] = 'xiaolu'
# print('-----------------修改之后的数据---------------------')
# for temp in list_dome:
#     print(temp)

list_dome = ['xiaoming1', 'xiaohong']
print('-----------------修改之前的数据---------------------')
for temp in list_dome:
    print(temp)
del list_dome[0]  # 删除 list_dome 中的数据
# list_dome.pop()# 删除 list_dome 中的最后一个元素
# list_dome.remove('xiaoming1')# 删除 list_dome 中的指定元素
print('-----------------修改之后的数据---------------------')
for temp in list_dome:
    print(temp)

list_dome = [3, 4, 5, 1, 7, 2]
list_dome.reverse()  # 将列表逆置
# list_dome.sort()#将列表由小到大排列
# list_dome.sort(reverse=True)#将列表由大到小排列
print(list_dome)

