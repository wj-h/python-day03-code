import random
# 列表嵌套
off = [[], [], []]
names = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']
for name in names:
    index = random.randint(0, 2)
    off[index].append(name)
i = 1
for temp in off:
    print('办公室%d的人数为:%d' % (i, len(temp)))
    i += 1
    for name in temp:
        print("%s" % name, end='')
    print("-" * 20)
