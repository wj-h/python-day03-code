# 元组
# tup1 = (1, 2, 3, 4, 5, 6, 7, 8, 9)
#
# # 查找元组
# tup1 = ('Google', 'Runoob', 1997, 2000)
# tup2 = (1, 2, 3, 4, 5, 6, 7)
# print("tup1[0]: ", tup1[0])
# print("tup2[1:5]: ", tup2[1:5])
#
# # 修改元组
# tup1 = (12, 34.56)
# tup2 = ('abc', 'xyz')
# # 以下修改元组元素操作是非法的。
# # tup1[0] = 100
# # 创建一个新的元组
# tup3 = tup1 + tup2
# print(tup3)
#
# # 删除元组
# tup = ('Google', 'Runoob', 1997, 2000)
# print(tup)
# del tup
# print("删除后的元组 tup : ")
# print(tup)
#
# # 元组内置函数
# len()  # 返回元组中元素的个数
# max()  # 返回元组中的最大值
# min()  # 返回元组中的最小值
# tuple()  # 将可迭代系列转换为元组,如下:
# list1 = ['Google', 'Taobao', 'Runoob', 'Baidu']
# tuple1 = tuple(list1)
# tuple1
# ('Google', 'Taobao', 'Runoob', 'Baidu')

# 字典
dict1 = {'name': 'Taobao', 'ege': 18}
# print(str(dict1))
# 输出字典，可以打印的字符串表示。
# 1	dict.clear()
# 删除字典内所有元素
# dict.copy()
# 返回一个字典的浅复制
# 3	dict.fromkeys()
# 创建一个新字典，以序列seq中元素做字典的键，val为字典所有键对应的初始值
# 4	dict.get(key, default=None)
# 返回指定键的值，如果键不在字典中返回 default 设置的默认值
# 5	key in dict
# 如果键在字典dict里返回true，否则返回false
# 6	dict.items()
# 以列表返回一个视图对象
# 7	dict.keys()
# 返回一个视图对象
# 8	dict.setdefault(key, default=None)
# 和get()类似, 但如果键不存在于字典中，将会添加键并将值设为default
# 9	dict.update(dict2)
# 把字典dict2的键/值对更新到dict里
# 10	dict.values()
# 返回一个视图对象
# 11	pop(key[,default])
# 删除字典 key（键）所对应的值，返回被删除的值。
# 12	popitem()
# 返回并删除字典中的最后一对键和值。
