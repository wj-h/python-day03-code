import tkinter as tk
import random


# 初始化游戏
def init_game():
    global canvas, score, snake, food, dx, dy
    canvas = tk.Canvas(root, width=400, height=400, bg='white')
    canvas.pack()
    score = 0
    snake = [[200, 200], [190, 200], [180, 200]]  # 蛇的初始位置
    food = [random.randint(0, 39), random.randint(0, 39)]  # 食物的初始位置
    dx, dy = 10, 0  # 蛇的初始移动方向
    update_game()


# 更新游戏状态
def update_game():
    global score, snake, food
    canvas.delete("all")  # 清除画布上的所有元素
    for y in range(0, 400, 10):  # 绘制网格F
        for x in range(0, 400, 10):
            canvas.create_line(x, y, x, y + 10, fill='black', width=1)
            canvas.create_line(x, y, x + 10, y, fill='black', width=1)
    for segment in snake:
        canvas.create_rectangle(segment[0], segment[1], segment[0] + 10, segment[1] + 10, fill='green')
    canvas.create_rectangle(food[0] * 10, food[1] * 10, food[0] * 10 + 10, food[1] * 10 + 10, fill='red')
    if snake[0] == food:
        score += 1
        food = [random.randint(0, 39), random.randint(0, 39)]
    move_snake()


# 移动蛇
def move_snake():
    global snake, dx, dy
    new_head = [snake[0][0] + dx, snake[0][1] + dy]
    snake.insert(0, new_head)
    if not (0 <= new_head[0] <= 390 and 0 <= new_head[1] <= 390):
        root.quit()
    for i in range(1, len(snake)):
        if new_head == snake[i]:
            root.quit()
    snake.pop()


# 改变方向
def change_direction(event):
    global dx, dy
    if event.keysym == 'Left' and dx != 10:
        dx, dy = -10, 0
    elif event.keysym == 'Right' and dx != -10:
        dx, dy = 10, 0
    elif event.keysym == 'Up' and dy != 10:
        dx, dy = 0, -10
    elif event.keysym == 'Down' and dy != -10:
        dx, dy = 0, 10


# 创建主窗口
root = tk.Tk()
root.title("Snake Game")
root.bind("<KeyPress>", change_direction)

init_game()

root.mainloop()
