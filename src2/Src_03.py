import pygame
import random

pygame.init()

# 设置窗口大小和标题
window_width = 600
window_height = 600
window = pygame.display.set_mode((window_width, window_height))
pygame.display.set_caption('贪吃蛇游戏')

# 定义颜色
black = (0, 0, 0)
white = (255, 255, 255)
green = (0, 255, 0)
red = (255, 0, 0)

# 设置方块大小和速度
block_size = 20
speed = 20

clock = pygame.time.Clock()

# 初始化得分和计时器
score = 0
start_time = pygame.time.get_ticks()

font = pygame.font.SysFont(None, 25)


def draw_snake(snake_list):
    for x in snake_list:
        pygame.draw.rect(window, black, [x[0], x[1], block_size, block_size])


def draw_score():
    # 计算游戏时间（秒）
    elapsed_time = (pygame.time.get_ticks() - start_time) / 1000.0
    # 渲染得分和时间
    score_text = font.render(f"Score: {score}", True, black)
    time_text = font.render(f"Time: {int(elapsed_time)}s", True, black)
    window.blit(score_text, [10, 10])
    window.blit(time_text, [10, 40])


def game_over_msg():
    text = font.render("Game Over!", True, red)
    window.blit(text, [window_width / 2 - 60, window_height / 2])


def gameLoop():
    global score
    game_over = False

    lead_x = window_width / 2
    lead_y = window_height / 2
    lead_x_change = speed
    lead_y_change = 0

    snake_list = [[lead_x, lead_y]]
    length_of_snake = 1

    randAppleX = round(random.randrange(0, window_width - block_size) / 20) * 20
    randAppleY = round(random.randrange(0, window_height - block_size) / 20) * 20

    while not game_over:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                game_over = True
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    lead_x_change = -speed
                    lead_y_change = 0
                elif event.key == pygame.K_RIGHT:
                    lead_x_change = speed
                    lead_y_change = 0
                elif event.key == pygame.K_UP:
                    lead_y_change = -speed
                    lead_x_change = 0
                elif event.key == pygame.K_DOWN:
                    lead_y_change = speed
                    lead_x_change = 0

        lead_x += lead_x_change
        lead_y += lead_y_change

        if lead_x >= window_width or lead_x < 0 or lead_y >= window_height or lead_y < 0:
            game_over = True

        window.fill(white)
        pygame.draw.rect(window, red, [randAppleX, randAppleY, block_size, block_size])
        draw_score()

        snake_head = [lead_x, lead_y]
        snake_list.append(snake_head)
        if len(snake_list) > length_of_snake:
            del snake_list[0]

        for x in snake_list[:-1]:
            if snake_head == x:
                game_over = True

        draw_snake(snake_list)

        pygame.display.update()
        clock.tick(10)

        if lead_x == randAppleX and lead_y == randAppleY:
            randAppleX = round(random.randrange(0, window_width - block_size) / 20) * 20
            randAppleY = round(random.randrange(0, window_height - block_size) / 20) * 20
            length_of_snake += 1
            score += 1  # 增加得分

    game_over_msg()
    pygame.display.update()
    pygame.time.wait(2000)  # 等待2秒后自动关闭游戏
    pygame.quit()
    quit()


gameLoop()
