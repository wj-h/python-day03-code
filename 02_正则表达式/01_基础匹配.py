"""
re.match只匹配开头
re.search匹配全局的第一个
re.findall匹配全局的所有
"""
import re

a = "python is big python 666"
result = re.match('python', a)
print(result)
print(result.span())
print(result.group())
