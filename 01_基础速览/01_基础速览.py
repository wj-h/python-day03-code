import random

a = [1, 2, 3, 4]  # 列表
b = {1, 2, 3, 4}  # 集合
c = (1, 2, 3, 4)  # 元组
d = {'n': 1, 'b': 2}  # 字典

e = 12
print(f"hello {e}")
print("hello %d" % e)

print("he\nllo %d")  # 换行
print("he\tllo %d")  # 长空格
print("he1llo %d", end='')  # 不换行
print("he1llo %d")

"""
f = input("请输入字符串")
print(f"你输入的字符串事:{f}")
g = int(input("请输入数字"))
print(f"你输入的数字是:{g}")
"""
h = 15
m = str(h)
i = float(m)

aa = random.randint(1, 100)  # 随机获取一个整数
bb = random.uniform(1, 100)  # 随机获取一个小数
cc = random.random()
print(aa)
print(bb)
print(cc)

# 修改变量
dd = 1
dd = dd + 1  # dd +=1
print(dd)
print()
# and or not(与 或 非)

"""
if判断
while循环(break,continue(跳过))
for循环
"""
for ee in range(5):
    print(ee)

# 字符串切片
ff = "my name is xxx"
# ff = ff[1:5]  # 下标从1到5
# ff = ff[::-1]  # 把字符串反过来
# print(ff)

# ff =ff.replace("xxx","pig")
arr = ff.split(" ")
